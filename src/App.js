
import { useState } from 'react';

import { Line, Between, Center } from "./UIkit/Layouts/Line/Line";
import { GridRows } from "./UIkit/Layouts/Grid/Grid";

import './App.css';
import { Icon } from "./UIkit/Elements/Icon/Icon";
import { LoginView } from "./Views/Login";

import { Routes, Route } from 'react-router-dom';
import { Link, NavLink } from 'react-router-dom';

import { AboutView } from "./Views/About";
import CounterFunc from "./Components/Counter";
import { DropDownView } from "./Views/DropDownView";
import HooksView from "./Views/Hooks";
import AxiosView from "./Views/Axios";
import ContextView from "./Views/Context";

import { counterContext } from './Context/counterContext';
import ReduxView from './Views/Redux';
import CallbackView from './Views/Callback';

const App = () => {
    const [count, setCount] = useState(0);

    return (
        <div className="App">
            <counterContext.Provider value={{ count, setCount }}>
                <GridRows>
                    <Between>
                        <Line>
                            <div>React</div>
                            <Icon i="heart" />
                        </Line>
                        <Line>
                            <NavLink to="/login">login</NavLink>
                            <NavLink to="/dropdown">dropdown</NavLink>
                            <NavLink to="/hooks">hooks</NavLink>
                            <NavLink to="/axios">axios</NavLink>
                            <NavLink to="/context">context</NavLink>
                            <NavLink to="/redux">redux</NavLink>
                            <NavLink to="/callback">callback</NavLink>
                        </Line>
                    </Between>
                    <Center>
                        <Routes>
                            <Route path="/" element={<h1>Home</h1>} />
                            <Route path="/login" element={<LoginView />} />
                            <Route path="/about" element={<AboutView />} />
                            <Route path="/dropdown" element={<DropDownView />} />
                            <Route path="/hooks" element={<HooksView />} />
                            <Route path="/axios" element={<AxiosView />} />
                            <Route path="/context" element={<ContextView />} />
                            <Route path="/redux" element={<ReduxView />} />
                            <Route path="/callback" element={<CallbackView />} />
                        </Routes>
                    </Center>
                </GridRows>
            </counterContext.Provider>
        </div>
    )
}

export default App;