import './Box.css';

export const Box = ({title, children}) => {
    return (
        <div className='Box'>
            {title && <h2>{title}</h2>}
            <div className='content'>
                {children}
            </div>
        </div>
    )
}