import './Grid.css';

export const Grid = ({ className, children }) => {
    return (
        <div className={`Grid ${className || ''}`}>
            {children.map((i, index) => (
                <div key={index}>{i}</div>
            ))}
        </div>
    )
}

export const GridRows = (props) => {
    return <Grid {...props} className="rows" />
}

export const GridCols = (props) => {
    return <Grid {...props} className="cols" />
}