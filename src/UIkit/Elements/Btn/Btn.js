import { Between } from '../../Layouts/Line/Line';
import { Icon } from '../Icon/Icon';
import './Btn.css';

export const Btn = ({ onClick, children, i }) => {
    return (
        <button className="Btn" onClick={onClick}>
            <Between>
             {children || 'Btn'}
             {i && <Icon i={i} />}
            </Between>
            
        </button>
    )
}