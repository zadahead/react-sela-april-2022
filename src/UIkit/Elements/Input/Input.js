import { Between } from '../../Layouts/Line/Line';
import { Icon } from '../Icon/Icon';
import './Input.css';

export const Input = ({ i, ...props}) => {
    return (
        <div className="Input">
           <Between>
                <input {...props} />
                {i && <Icon i={i} />}
           </Between>
        </div>
    )
}