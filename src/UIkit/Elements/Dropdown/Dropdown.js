import { useEffect, useRef, useState } from "react";
import { Between } from "../../Layouts/Line/Line"
import { Icon } from "../Icon/Icon"

import './Dropdown.css';
export const Dropdown = ({ list, selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);
    const wrapperRef = useRef();


    useEffect(() => {
        window.addEventListener('click', handleBodyClick);
        return () => {
            window.removeEventListener('click', handleBodyClick);
        }
    }, [])

    const handleToggle = () => {
        
        setIsOpen(!isOpen);
    }

    const handleBodyClick = (e) => {
        console.log('handleBodyClick');

        if(!wrapperRef.current) { return; }

        if(!wrapperRef.current.contains(e.target)) {
            setIsOpen(false);
        }
    }

    const handleSelected = (item) => {
        console.log('handleSelected', item);
        onChange(item.id);
        setIsOpen(false);
    }
    const getSelected = () => {
        return list.find(i => i.id === selected);
    }
    const renderTitle = () => {
        const item = getSelected();
        if(item) {
            return item.name;
        }
        return 'Please Select';
    }

    return (
        <div className="Dropdown" ref={wrapperRef}>
            <div className="trig" onClick={handleToggle}>
                <Between>
                    <h4>{renderTitle()}</h4>
                    <Icon i="chevron-down" />
                </Between>
            </div>
            {
                isOpen &&
                (
                    <div className="list">
                       {list.map(i => (
                           <div key={i.id} onClick={() => handleSelected(i)}>{i.name}</div>
                       ))}
                    </div>
                )
            }
        </div>
    )
}