import { useFetch } from "../../Hooks/useFetch";

export const Fetch = (props) => {
    //logic
    const { list, error } = useFetch(props.url);

    //render
    if (error) {
        return <h3 style={{ color: 'red' }}>{error}</h3>
    }

    if (!list) {
        return <h3>loading...</h3>
    }

    return (
        list.splice(0, 5).map(i => props.onRender(i))
    );
}