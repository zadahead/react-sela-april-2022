/*
componentDidMount = []
componentDidUpdate = no dep
componentWillUnmount
render
*/

import { useEffect, useState } from "react";

export const FuncLifeCycle = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [count, setCount] = useState(0);

    console.log('render');

    useEffect(() => {
        console.log('componentDidMount');
        setTimeout(() => {
            setIsLoading(false);
        }, 2000);

        return () => {
            console.log('componentWillUnmount');
        }
    }, [])   
    
    useEffect(() => {
        console.log('componentDidUpdate', count);

        return () => {
            console.log('count unmount', count);
        }
    }, [count])

    const handleAdd = () => {
        setCount(count + 1);
    }

    if(isLoading) {
        return (
            <div>
                <h3>loading.....</h3>
            </div>
        )
    }
    return (
        <div>
            <h3>FuncLifeCycle, {count}</h3>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}