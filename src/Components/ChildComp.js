import React from 'react';

const ChildComp = ({ title, calc }) => {
    console.log('<ChildComp />');

    return (
        <div>
            <h1>ChildComp, { title }, {calc(10, 2)}</h1>
        </div>
    )
}

export default React.memo(ChildComp);