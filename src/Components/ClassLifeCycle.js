import React from "react";

/*
componentDidMount
componentDidUpdate
componentWillUnmount
render
*/

export class ClassLifeCycle extends React.Component {
    state = {
        isLoading: true
    }
    componentDidMount = () => {
        console.log('componentDidMount');
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 2000);
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate')
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    render = () => {
        console.log('render');
        if(this.state.isLoading) {
            return (
                <div>
                    <h3>loading....</h3>
                </div>
            )
        }
        return (
            <div>
                <h1>ClassLifeCycle</h1>
            </div>
        )
    }
}