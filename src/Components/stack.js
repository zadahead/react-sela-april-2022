
import React, { useState } from 'react';
/*

        <div>
            <h1>Welcome</h1>
            <div>
                <h2>This is react</h2>
            </div>
        </div>

*/

const Welcome = (props) => {

    const logMessage = () => {
        console.log(`The Message is: ${props.message}`);
    }

    return (
        <>
            <h2>Welocome {props.name}</h2>
            <button onClick={logMessage}>Do Something</button>
        </>
    )
}

class WelcomeClass extends React.Component {

    logMessage = () => {
        console.log(`The Message is: ${this.props.message}`);
    }

    render = () => {

        return (
            <>
                <h1>Welcome Class</h1>
                <button onClick={this.logMessage}>Log From Class</button>
                <button onClick={this.props.func}>Trigger Func</button>
                <div>
                    <Welcome name={this.props.name} message={this.props.message} />
                </div>
            </>
        )
    }
}

const someLog = () => {
    console.log('this is some log');
}




class CounterClass extends React.Component {
    state = {
        count: 10
    }

    handleAdd = () => {
        console.log('handleAdd');
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        console.log('render');

        const styleCss = {
            color: 'red',
            backgroundColor: 'yellow'
        }

        return (
            <div>
                <h1 style={styleCss}>Count: {this.state.count}</h1>
                <button onClick={this.handleAdd}>Add</button>
            </div>
        )
    }
}


class ColorSwitcher extends React.Component {
    state = {
        isRed: true
    }

    handleSwitch = () => {
        this.setState({
            isRed: !this.state.isRed
        })
    }

    render = () => {

        const styleCss = {
            color: this.state.isRed ? 'red' : 'blue'
        }

        return (
            <div>
                <h1 style={styleCss}>Color Switcher</h1>
                <button onClick={this.handleSwitch}>Switch</button>
            </div>
        )
    }
}


class CounterSwitcherClass extends React.Component {
    state = {
        count: 0,
        isRed: true
    }

    handleAdd = () => {
        this.setState({
            count: this.state.count + 1
        })
    }


    handleSwitch = () => {
        this.setState({
            isRed: !this.state.isRed
        })
    }

    handleCombine = () => {
        this.setState({
            isRed: !this.state.isRed,
            count: this.state.count + 1
        })
    }

    render = () => {

        const styleCss = {
            color: this.state.isRed ? 'red' : 'blue'
        }

        return (
            <div>
                <div>
                    <h1 style={styleCss}>Count: {this.state.count}</h1>
                </div>
                <button onClick={this.handleAdd}>Add</button>
                <button onClick={this.handleSwitch}>Switch</button>
                <button onClick={this.handleCombine}>Combine</button>
            </div>
        )
    }
}



const ColorSwitcherFunc = () => {
    const [isRed, setIsRed] = useState(false);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }
    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>Color Switcher</h1>
            <button onClick={handleSwitch}>Switch</button>
        </div>
    )
}

const Combine = () => {
    console.log('render');

    const [isRed, setIsRed] = useState(false);
    const [count, setCount] = useState(10);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const handleCombine = () => {
        setCount(count + 1);
        setIsRed(!isRed);
    }
    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>Combine Switcher Count, {count}</h1>
            <button onClick={handleSwitch}>Switch</button>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleCombine}>TriggerBoth</button>
        </div>
    )
}



const CounterWrap = () => {
    const [count, setCount] = useState(10);

    return (
        <div>
            <CounterFunc count={count} setCount={setCount} />
            <CounterFunc count={count} setCount={setCount} />
            <CounterFunc count={count} setCount={setCount} />
        </div>
    )
}

const CounterFunc = (props) => {

    const handleAdd = () => {
        props.setCount(props.count + 1);
    }

    return (
        <div>
            <h1>Counter, {props.count}</h1>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}



/*
initialState
state
setState
render
*/

const User = (props) => {
    const [isShow, setIsShow] = useState(false);

    const handleToggle = () => {
        setIsShow(!isShow);
    }

    const renderButtonText = () => {
        if (isShow) {
            return 'Hide Age'
        }
        return 'Show Age'
    }

    return (
        <div>
            <h2>User: {props.name}</h2>
            {isShow && <h3>Age: {props.age}</h3>}
            <button onClick={handleToggle}>{renderButtonText()}</button>
        </div>
    )
}

const UsersList = () => {
    return (
        <div>
            <h1>Users List:</h1>
            <div>
                <User name="Mosh" age={22} />
                <User name="David" age={32} />
                <User name="Sis" age={43} />
            </div>
        </div>
    )
}