import { useContext } from "react"
import { counterContext } from "../Context/counterContext"

export const CounterDisplay = () => {
    const { count } = useContext(counterContext);

    return (
        <div>
            <h3 style={{ color: '#6c8ed6'}}>CounterDisplay, {count}</h3>
        </div>
    )
}