export const Wrapper = (props) => {
    console.log(props);

    const styleCss = {
        backgroundColor: 'gray'
    }
    
    return (
        <div style={styleCss}>
            <h1>This is a wrapper</h1>
            {props.children}
        </div>
    )
}



/*

    <Box title="this is a title">
        <>....</>
    </Box>

*/