import { Btn } from '../UIkit/Elements/Btn/Btn';

import { useDispatch } from 'react-redux';
import { Line } from '../UIkit/Layouts/Line/Line';

const CounterReduxAdd = () => {
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(async () => {
            return {
                type: 'ADD_COUNT',
                payload: 2
            }
        })
    }

    const handleMulti = () => {
        dispatch({
            type: 'MUL_COUNT',
            payload: 2
        })
    }

    const handleSub = () => {
        dispatch({
            type: 'SUB_COUNT',
            payload: 1
        })
    }
    return (
        <div>
            <h2>CounterReduxAdd</h2>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleMulti}>Multi</Btn>
                <Btn onClick={handleSub}>Sub</Btn>
            </Line>
        </div>
    )
}

export default CounterReduxAdd;