import { useState } from "react";

export const CounterFunc = () => {
    const [count, setCount] = useState(10);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <div>
            <h4>Counter, {count}</h4>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}

export default CounterFunc