import { useState } from "react";

export const ColorSwitcher = () => {
    const [isRed, setIsRed] = useState(false);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }
    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>Color Switcher</h1>
            <button onClick={handleSwitch}>Switch</button>
        </div>
    )
}