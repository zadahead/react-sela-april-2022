
import { User } from "./User"
const list = [
    { id: 1, name: 'Mosh'},
    { id: 2, name: 'David'},
    { id: 3, name: 'Sis'}
]

export const Users = () => {
    return (
        <div>
            <h1>Users</h1>
            <div>
                {list.map((i) => <User key={i.id} name={i.name} />)}
            </div>
        </div>
    )
} 