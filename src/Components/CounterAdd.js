import { useContext } from "react"
import { counterContext } from "../Context/counterContext"
import { Btn } from '../UIkit/Elements/Btn/Btn';

export const CounterAdd = () => {
    const { count, setCount } = useContext(counterContext);
    
    const handleAdd = () => {
        setCount(count + 1);
    }
    return (
        <div>
            <h3>CounterAdd</h3>
            <Btn onClick={handleAdd}>Add</Btn>
        </div>
    )
}