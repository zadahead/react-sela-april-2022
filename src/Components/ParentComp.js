import { useCallback, useState } from "react";
import { Btn } from "../UIkit/Elements/Btn/Btn";
import ChildComp from "./ChildComp";

const ParentComp = () => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const calc = useCallback((a, b) => {
        return a + b;
    }, [count])

    console.log('<ParentComp />');

    return (
        <div>
            <h1>ParentComp, {count}</h1>
            <Btn onClick={handleAdd}>Add</Btn>
            <ChildComp title="hello" calc={calc} />
        </div>
    )
}

export default ParentComp;