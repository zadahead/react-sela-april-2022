export const Box = ({ title, children }) => {

    const styleCss = {
        backgroundColor: '#e1e1e1',
        padding: '10px'
    }

    return (
        <div>
            <h2>{title}</h2>
            <div style={styleCss}>
                {children}
            </div>
        </div>
    )
}