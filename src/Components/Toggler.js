import { useState } from "react"

export const Toggler = (props) => {
    const [isDisplay, setIsDisplay] = useState(true);

    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }

    const renderComponent = () => {
        if(isDisplay) {
            return props.comp;
        }
        return null;
    }

    return (
        <div>
            <h1>Toggler</h1>
            <button onClick={handleToggle}>Toggle</button>

            <div>
                {renderComponent()}
            </div>
        </div>    
    )
}