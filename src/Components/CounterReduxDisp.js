import { useSelector } from 'react-redux';

const CounterReduxDisp = () => {
    const count = useSelector((state) => {
        return state.count
    });

    return (
        <div>
            <h2>CounterReduxDisp, {count}</h2>
        </div>
    )
}

export default CounterReduxDisp;