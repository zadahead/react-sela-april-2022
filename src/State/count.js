export const countReducer = (count = 0, action) => {
    switch(action.type) {
        case "ADD_COUNT": return count + action.payload;
        case "MUL_COUNT": return count * action.payload;
        case "SUB_COUNT": return count - action.payload;
        default: return count;
    }
}