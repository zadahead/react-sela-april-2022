
import { createStore, combineReducers } from "redux";

import { countReducer } from './count';

const reducers = combineReducers({
    count: countReducer
});

const store = createStore(reducers);

export default store;