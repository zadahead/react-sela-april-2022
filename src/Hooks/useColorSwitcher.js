import { useState } from "react";

export const useColorSwitcher = () => {
    const [isRed, setIsRed] = useState(false);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }
    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return {
        styleCss,
        handleSwitch
    }
}