import axios from "axios";
import { useEffect, useState } from "react";

export const useFetch = (url) => {
    const [list, setList] = useState(null);
    const [error, setError] = useState('');

    useEffect(() => {
        const source = axios.CancelToken.source();
        const config = { 
            cancelToken: source.token
        }
        axios.get(url, config)
            .then(resp => {
                setList(resp.data);
            })
            .catch(err => {
                if(axios.isCancel(err)) {
                    console.log('Axios Cancel');
                }
                else {
                    setError(err.message);
                }
            })

        return () => {
            source.cancel();
        }
    }, [])

    return {
        list,
        error
    }
}