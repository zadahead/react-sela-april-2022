import { useEffect, useRef } from "react"
import { Btn } from "../UIkit/Elements/Btn/Btn";

export const AboutView = () => {
    const wrapperRef = useRef();
    const someValueRef = useRef();

    someValueRef.current = 10;

    const handlePaint = () => {
        wrapperRef.current.style.backgroundColor = 'green';
    }
    useEffect(() => {
        setTimeout(() => {
            console.log(someValueRef);
        }, 2000);
    }, [])

    return (
        <div>
            <h2>About Page</h2>
            <div ref={wrapperRef}>wrapper</div>
            <Btn onClick={handlePaint}>Paint</Btn>
        </div>
    )
}