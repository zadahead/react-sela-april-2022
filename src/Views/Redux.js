import CounterReduxAdd from "../Components/CounterReduxAdd";
import CounterReduxDisp from "../Components/CounterReduxDisp"

const ReduxView = () => {
    return (
        <div>
            <h3>ReduxView</h3>
            <CounterReduxDisp />
            <CounterReduxAdd />
        </div>
    )
}

export default ReduxView;