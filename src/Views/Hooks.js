import { useColorSwitcher } from "../Hooks/useColorSwitcher";
import { useCounter } from "../Hooks/useCounter";
import { Btn } from "../UIkit/Elements/Btn/Btn";



const HooksView = () => {
    //logic
    const { count, handleAdd } = useCounter();
    const { styleCss, handleSwitch} = useColorSwitcher();

    //render
    return (
        <div>
            <h2 style={styleCss}>Hooks View, Count, {count}</h2>
            <Btn onClick={handleSwitch}>Switch</Btn>
            <Btn onClick={handleAdd}>Add</Btn>
        </div>
    )
}
export default HooksView;