import ParentComp from "../Components/ParentComp"

const CallbackView = () => {
    return (
        <div>
            <ParentComp />
        </div>
    )
}

export default CallbackView;