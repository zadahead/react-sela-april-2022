import { CounterAdd } from "../Components/CounterAdd";
import { CounterDisplay } from "../Components/CounterDisplay";
import { Rows } from "../UIkit/Layouts/Line/Line";


const ContextView = () => {
    
    return (
        <div>
            <Rows>
                <h3>Context View</h3>
                <CounterAdd />
                <CounterDisplay />
            </Rows>
        </div>
    )
}

export default ContextView;