
import { useInput } from "../Hooks/useInput";

import { Rows } from "../UIkit/Layouts/Line/Line";

import { Btn } from "../UIkit/Elements/Btn/Btn";
import { Input } from "../UIkit/Elements/Input/Input";
import { Box } from "../UIkit/Layouts/Box/Box";
import { CounterDisplay } from "../Components/CounterDisplay";


export const LoginView = () => {
    const username = useInput('', 'username');
    const password = useInput('', 'password');

    const handleClick = () => {
        console.log(username.value, password.value);
    }

    return (
        <Box title="Login">
            <Rows>
                <Input i="user" {...username} />
                <Input i="key" {...password} />
                <Btn onClick={handleClick}>Get Value</Btn>
                <CounterDisplay />
            </Rows>
        </Box>
    )
}