
import { Fetch } from "../UIkit/Components/Fetch";


const AxiosView = () => {

    return (
        <div>
            <h2>Axios View</h2>
            <div>
                <Fetch
                    url="https://jsonplaceholder.typicode.com/todos"
                    onRender={i => (
                        <h4 key={i.id}>{i.title}</h4>
                    )}
                />
            </div>
        </div>

    )
}

export default AxiosView;