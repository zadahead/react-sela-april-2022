import { useState } from "react"
import { Dropdown } from "../UIkit/Elements/Dropdown/Dropdown"
import { Box } from "../UIkit/Layouts/Box/Box"

const list = [
    { id: 1, name: 'mosh'},
    { id: 2, name: 'david'},
    { id: 3, name: 'sis'}
]

export const DropDownView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <div>
            <Box title="drop down">
                <Dropdown list={list} selected={selected} onChange={setSelected} />
            </Box>
        </div>
    )
}